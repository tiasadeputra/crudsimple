(function() {
    'use strict';

    angular
        .module('crudSederhanaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
